# 143-yii2-app-basic

## what

* composer创建的一个yii2项目模板

## how

### 1 创建项目

```
composer create-project --prefer-dist yiisoft/yii2-app-basic 143-yii2-app-basic -vvv
```

### 2 发布

* 登录 https://packagist.org
* submit `https://gitlab.com/do365-public/143-yii2-app-basic.git`
* submit时一定要填https地址，要不Details链接无法访问

### 3 发布检查

* https://packagist.org/packages/do365/143-yii2-app-basic

### 4 自动发布

* gitlab do365-public 143-yii2-app-basic Settings Integrations Packagist
* 输入username token

## 开发流程

### 1 安装依赖

```
composer install --no-interaction --ansi
composer update --no-interaction --ansi
```